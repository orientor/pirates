from colours import *
try:
    from conf import *
except:
    from sampleconf import *
from goblin import *
import os
from player import player
import pygame
from pygame.locals import *
import random
import sys
from timeit import default_timer as timer

main_clock = pygame.time.Clock()
shipimg = pygame.image.load("ship.png")
pygame.init()

window_surface = pygame.display.set_mode((WIDTH, HEIGHT))
window_rect = pygame.Rect(0, 0, WIDTH, HEIGHT)

CELLSIZE = 10

COLUMNS = WIDTH // CELLSIZE
ROWS = HEIGHT // CELLSIZE

grid = []

for row in range(1, ROWS):
    for column in range(1, COLUMNS):
        coordinate = (column, row)
        grid.append(coordinate)

# each coordinate will correspond to one cell in the grid; we will multiply by
# 10

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'
max_y = 0
min_y = ROWS-1
max_score1=0
max_score2 = 0
p1t=[]
p2t=[]
p1s=[]
p2s=[]

pygame.mouse.set_visible(False)

basic_font = pygame.font.SysFont(None, 20)
small_font = pygame.font.SysFont(None, 20)
largeFont = pygame.font.SysFont('comicsans', 42)
explanation = ""
roundno=1
exp_surf = small_font.render(explanation, 1, WHITE)
exp_rect = exp_surf.get_rect()
exp_rect.topleft = (COLUMNS//3 * CELLSIZE, 0)
def get_s(a):
    if a==0:
        return 0
    if a==1:
        return 5
    if a==2:
        return 15
    if a==3:
        return 20
    if a==4:
        return 30
    if a==5:
        return 35
    if a==6:
        return 45
    if a==7:
        return 50
    if a==8:
        return 60
    if a==9:
        return 65
    if a==10:
        return 75

def render_multi_line(text, x, y, fsize):
    lines = text.splitlines()
    for i, l in enumerate(lines):
        window_surface.blit(largeFont.render(l, 0, WHITE), (x, y + fsize*i))

# moves character as long as they will not end up offscreen
def end_score_game():
    # another game loop
    run = True
    while run:
        pygame.time.delay(100)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                end_game()
            if event.type == KEYDOWN: # if the user hits the mouse button
                end_game()

        # This will draw text displaying the score to the screen.
        window_surface.fill(BLACK)
        text="Player Score Time\nRound 1\n1     %s   %s\n2     %s   %s\n"%(str(p1s[0]),str(round(p1t[0],2)),str(p2s[0]),str(round(p2t[0],2)))# creates a font object
        text+="Round 2\n1     %s   %s\n2     %s   %s\n"%(str(p1s[1]),str(round(p1t[1],2)),str(p2s[1]),str(round(p2t[1],2)))# creates a font object
        render_multi_line(text,1,1,42)

        pygame.display.update()

def move_character(chara, direction):
    if direction == UP:
        if chara.location[1] - chara.movespeed > -chara.movespeed:
            chara.location[1] = chara.location[1] - chara.movespeed

    elif direction == DOWN:
        if chara.location[1] + chara.movespeed < ROWS:
            chara.location[1] = chara.location[1] + chara.movespeed

    elif direction == LEFT:
        if chara.location[0] - chara.movespeed > -chara.movespeed:
            chara.location[0] = chara.location[0] - chara.movespeed

    elif direction == RIGHT:
        if chara.location[0] + chara.movespeed < ROWS:
            chara.location[0] = chara.location[0] + chara.movespeed

    return chara.location


def end_game():

    pygame.quit()
    sys.exit()


def main():
    startt=timer()
    global roundno
    player.location = [COLUMNS/2, ROWS-1]  # player starts at bottom
    player_loc = player.location
    global max_y
    global min_y
    global max_score1
    global max_score2
    global p1t
    global p2t
    global p1s
    global p2s
    score = 0
    top_toggle = True

    for goblin in goblins:
        goblin.hit_right_wall = False

    direction = ''

    while True:

        player_x = player.location[0]
        player_y = player.location[1]
        max_y=max(max_y,player_y)
        min_y=min(min_y,player_y)
        player_rect = pygame.Rect(player_x, player_y, player.size, player.size)

        for event in pygame.event.get():
            if event.type == QUIT:
                end_game()
            if event.type == KEYDOWN:
                if top_toggle is True:
                    if event.key == K_ESCAPE:
                        end_game()
                    elif event.key == K_LEFT:
                        direction = LEFT
                    elif event.key == K_RIGHT:
                        direction = RIGHT
                    elif event.key == K_UP:
                        direction = UP
                    elif event.key == K_DOWN:
                        direction = DOWN
                else:
                    if event.key == K_ESCAPE:
                        end_game()
                    elif event.key == K_a:
                        direction = LEFT
                    elif event.key == K_d:
                        direction = RIGHT
                    elif event.key == K_w:
                        direction = UP
                    elif event.key == K_s:
                        direction = DOWN

            # stop if user releases key. we don't use keyup as they might
            # release a key while holding another down.
            elif 1 not in pygame.key.get_pressed():
                direction = ""
        player_loc = move_character(player, direction)
        lpcnt = 0
        for goblin in goblins:
            goblin_x = goblin.location[0]
            goblin_y = goblin.location[1]
            goblin_rect = pygame.Rect(goblin_x, goblin_y, goblin.size, goblin.size)
            lpcnt = lpcnt + 1
            if lpcnt%2!=0:
                if goblin.hit_right_wall is False:
                    goblin_dir = RIGHT
                    if goblin_x + goblin.movespeed >= COLUMNS:
                        goblin.hit_right_wall = True
                        goblin_dir = LEFT
                elif goblin.hit_right_wall is True:
                    goblin_dir = LEFT
                    if goblin_x - goblin.movespeed <= 0:
                        goblin.hit_right_wall = False

                goblin.location = move_character(goblin, goblin_dir)

            if goblin_rect.colliderect(player_rect):
                if top_toggle is True:
                    player.location[1]=0
                    endt=timer()
                    p2t.append(endt-startt)
                    p2s.append(score)
                    startt=timer()
                    top_toggle=False
                else:
                    player.location[1]=ROWS-1
                    endt=timer()
                    p1t.append(endt-startt)
                    p1s.append(score)
                    startt=timer()
                    roundno = roundno + 1
                    if roundno is 3:
                        end_score_game()
                    top_toggle=True

        # give player a point each time they cross screen.
        # increase game speed with each multiple of 10 points.
        # That's just a suggested difficulty mechanic; fine to change or omit.

        if player.location[1] == 0 and top_toggle is True:
            max_y=0
            endt=timer()
            p2t.append(endt - startt)
            p2s.append(score)
            startt=timer()
            if score % 10 == 0:
                for goblin in goblins:
                    goblin.movespeed += 10
            top_toggle = False
        elif player.location[1] == ROWS - 1 and top_toggle is False:
            min_y=ROWS-1
            endt=timer()
            p1t.append(endt-startt)
            p1s.append(score)
            startt=timer()
            roundno = roundno + 1
            if roundno is 3:
                end_score_game()
            if score % 10 == 0:
                for goblin in goblins:
                    goblin.movespeed += 10
            top_toggle = True
        if top_toggle is True:
            score=get_s( ((ROWS-1)-min_y)// (ROWS//9) )

        if top_toggle is False:
            score=get_s((max_y)//(ROWS//9))

        # draw here so screen isn't far behind logic
        window_surface.fill(BLUE)
        if top_toggle is True:
            playern=2
        else:
            playern=1
        score_surf = basic_font.render('Round: %d       Score: %d       Player : %d' % (roundno,score,playern), 1, BRIGHTRED)
        score_rect = score_surf.get_rect()
        score_rect.topleft = (1, 1)
        window_surface.blit(score_surf, score_rect)
        window_surface.blit(exp_surf, exp_rect)

        player_x_pos = player_x * CELLSIZE
        player_y_pos = player_y * CELLSIZE
        player_scaled = player.size * CELLSIZE
        player_dimensions = player_x_pos, player_y_pos, player_scaled, player_scaled
        pygame.draw.rect(window_surface, BRIGHTGREEN, (player_dimensions))

        for goblin in goblins:
            goblin_x = goblin.location[0]
            goblin_y = goblin.location[1]
            goblin_x_pos = goblin_x * CELLSIZE
            goblin_y_pos = goblin_y * CELLSIZE
            goblin_scaled = goblin.size * CELLSIZE
            goblin_dimensions = goblin_x_pos, goblin_y_pos, goblin_scaled, goblin_scaled
            window_surface.blit(shipimg, goblin_dimensions)

        pygame.display.update()
        main_clock.tick(FPS)


main()
