# If you would like to save your own config, make a copy of this file
# called 'conf.py', and change that.

# Window width in pixels
WIDTH = 500

# Window height in pixels
HEIGHT = 500

# Framerate; a higher value will result in a faster game
FPS = 40

# Background music, put music in game's music directory and change
# the empty string to something like 'music/example.mp3'

MUSIC = ''

# Save file. Game will read high score from here.

SAVEFILE = 'save.txt'
