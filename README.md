Basic 'Cross the river' game.

Two players compete against each other and we only display the score. The
result is left for their interpretation.

We have two types of obstacles: static and dynamic.

To run it, install python 3.6 and pygame. Clone this repo, cd to folder,
 and run: `python3 game.py`.


The difficulty needs fixing. It's too hard at the moment.

I'll hopefully clean up the code at some point.

