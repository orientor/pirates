
# A common colours file for config

WHITE = (255, 255, 255)
CYAN = (0, 255, 255)
MAGENTA = (255, 0, 255)
BLACK = (0, 0, 0)
ORANGE = (255, 175, 0)
BRIGHTRED = (255, 0, 0)
RED = (155, 0, 0)
PALEGREEN = (150, 255, 150)
BRIGHTGREEN = (0, 255, 0)
GREEN = (0, 155, 0)
BRIGHTBLUE = (0, 0, 255)
BLUE = (0, 0, 155)
PALEYELLOW = (255, 255, 150)
BRIGHTYELLOW = (255, 255, 0)
YELLOW = (155, 155, 0)
DARKGREY = (50, 50, 50)  # for '50' shades of grey
