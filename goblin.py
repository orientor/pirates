# This is messy; the logic should be split out from the entities sometime.

from character import Character
from colours import *

goblin_names = []
goblins = []
difficulty = 3


def make_goblin(goblin):
    goblin = Character()
    goblin.colour = YELLOW
    goblin.size = 3
    return goblin

for i in range(9):
    i = str(i)
    goblin_name = 'goblin' + i
    goblin_names.append(goblin_name)

for goblin in goblin_names:
    new_goblin = make_goblin(goblin)
    goblins.append(new_goblin)

counter = 1

for goblin in goblins:
    setattr(goblin, 'location', [counter*5, counter*5])
    setattr(goblin, 'movespeed', counter % difficulty+1)  # 3 speeds
    counter += 1
